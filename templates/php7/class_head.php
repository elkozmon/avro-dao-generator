<?php
{% if namespace.is_some() -%}
namespace {{ namespace.unwrap() }};
{%- endif %}

{% if phpdoc.is_some() -%}
/**
 * {{ phpdoc.unwrap() }}
 */
{%- endif %}

class {{ name }} {
