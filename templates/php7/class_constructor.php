{% for s in strings %}
    {{ s }}{% if !loop.last %},{% endif %}
{% endfor %}

    public function __construct(
    {% for s in strings %}
        {{ s }}{% if !loop.last %},{% endif %}
    {% endfor %}
    )
    {
    {% for s in strings %}
        {{ s }};
    {% endfor %}
    }
