
    public function get{{ name|capitalize }}(): {{ data_type }}
    {
        return $this->{{ name|lowercase }};
    }

    public function set{{ name|capitalize }}({{ data_type }} ${{ name|lowercase }})
    {
        $this->{{ name|lowercase }} = ${{ name|lowercase }};

        return $this;
    }
