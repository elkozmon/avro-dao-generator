extern crate avro_dao_generator;

use avro_dao_generator::Language;

#[test]
fn it_generates_single_dao() {
    let raw_schema = r#"
    {
        "type": "record",
        "name": "test",
        "fields": [
            {"name": "a", "type": "long", "default": 42},
            {"name": "b", "type": "string"}
        ]
    }
"#;

    let daos = avro_dao_generator::generate_from_schema(raw_schema, &Language::Php7).unwrap();

    println!("{:#?}", daos);
    assert_eq!(daos.len(), 1);
}
