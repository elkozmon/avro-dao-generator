use askama::Template;

#[derive(Template)]
#[template(path="php7/class_head.php")]
pub struct ClassHeadTemplate<'a> {
    pub name: &'a String,
    pub namespace: &'a Option<String>,
    pub phpdoc: &'a Option<String>
}

#[derive(Template)]
#[template(path="php7/class_tail.php")]
pub struct ClassTailTemplate {}

mod filters {

    use askama;
    use inflector::Inflector;

    pub fn capitalize(s: &str) -> askama::Result<String> {
        Ok(s.to_title_case())
    }
}
