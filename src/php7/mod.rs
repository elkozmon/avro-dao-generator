mod class;
mod template;

use self::class::Class;
use self::class::Field;
use dao::Dao;
use error::GeneratorError;

use std::env;

use avro_rs::Schema;
use avro_rs::schema::{Name, Documentation, RecordField};

use askama::Template;

pub fn generate_from_schema(schema: Schema, mut args: env::Args) -> Result<Vec<Box<dyn Dao>>, GeneratorError> {
    // TODO parse namespace from args

    if let Schema::Record { name, doc, fields, lookup: _ } = schema {
        class_from_record(name, namespace, doc, fields)
    } else {
        Err(GeneratorError::new("Invalid top-level schema type"))
    }
}

fn class_from_schema(schema: Schema) -> Result<Vec<Class>, GeneratorError> {

}

fn class_from_record(name: Name, namespace: Option<String>, doc: Documentation, fields: Vec<RecordField>) -> Result<Vec<Class>, GeneratorError> {
    let mut record_class = Class::new(name.name, namespace, doc);
    let mut classes = Vec::new();

    for field in fields.into_iter() {
        match field.schema {
            Schema::Null => {},
            Schema::Boolean =>
                record_class.add_field(
                    Field::Simple {
                        name: field.name,
                        data_type: "bool".to_string(),
                        default: field.default
                    }
                ),
            Schema::Int => {},
            Schema::Long => {},
            Schema::Float => {},
            Schema::Double => {},
            Schema::Bytes => {},
            Schema::String => {},
            Schema::Array(schema) => {

            },
            Schema::Map(schema) => {

            },
            Schema::Union(union_schema) => {

            },
            Schema::Record { name, doc, fields, lookup: _ } =>
                classes.append(class_from_record(name, namespace, doc, fields)?),
            Schema::Enum { name, doc, symbols } => {

            },
            Schema::Fixed { name, usize } => {

            },
        }
    }

    classes.push(record_class);

    Ok(classes)
}
