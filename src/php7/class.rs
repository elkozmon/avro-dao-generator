use dao::Dao;

pub enum Field {
    Simple { name: String, data_type: String, default: Option<String> },
    Union { name: String, data_type: Vec<String>, default: Option<String> },
    Fixed { name: String, size: u32, default: Option<String> }
}

pub struct Class {
    name: String,
    namespace: Option<String>,
    phpdoc: Option<String>,
    fields: Vec<Field>,
}

impl Class {
    pub fn new(name: String, namespace: Option<String>, phpdoc: Option<String>) -> Class {
        Class {
            name,
            namespace,
            phpdoc,
            fields: Vec::new()
        }
    }

    pub fn add_field(&mut self, field: Field) {
        unimplemented!()
    }
}

impl Dao for Class {
    fn name(&self) -> &str {
        &self.name + ".php"
    }

    fn data(&self) -> &str {
        unimplemented!()
    }
}
