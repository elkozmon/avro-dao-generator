use std::convert;
use askama;
use failure;

#[derive(Debug, Fail)]
#[fail(display = "Generator error: {}", message)]
pub struct GeneratorError {
    message: String
}

impl GeneratorError {
    pub fn new(message: &str) -> GeneratorError {
        GeneratorError { message: message.to_string() }
    }
}

impl convert::From<askama::Error> for GeneratorError {
    fn from(err: askama::Error) -> Self {
        GeneratorError::new(&format!("{}", err))
    }
}

impl convert::From<failure::Error> for GeneratorError {
    fn from(err: failure::Error) -> Self {
        GeneratorError::new(&format!("{}", err.as_fail()))
    }
}
