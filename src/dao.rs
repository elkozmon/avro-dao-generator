pub trait Dao {
    fn name(&self) -> &str;
    fn data(&self) -> &str;
}
