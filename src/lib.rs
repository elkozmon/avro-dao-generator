extern crate avro_rs;
extern crate inflector;
#[macro_use] extern crate askama;
#[macro_use] extern crate failure;

mod dao;
mod error;
mod php7;

use dao::Dao;
use error::GeneratorError;
use avro_rs::Schema;

#[derive(Debug)]
pub enum Language {
    Php7
}

pub fn generate_from_schema(raw_schema: &str, language: &Language) -> Result<Vec<Box<dyn Dao>>, GeneratorError> {
    let schema = Schema::parse_str(raw_schema)?;
    let args = std::env::args();

    match language {
        Language::Php7 => php7::generate_from_schema(schema, args)
    }
}
